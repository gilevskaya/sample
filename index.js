const express = require('express');

const app = express();
app.disable('x-powered-by');
app.set('json spaces', 2);

// app.use('/api', routes);
const port = process.env.PORT || 1337;
app.get('/', (_, res) => res.send('Hello world!'));
app.listen(port, () => console.log(`Listening on ${port}`));
